# Howto push data from server to client in Apache Wicket #

In company where I work we're considering to use **server-push pattern** to push data from server to client **when some asynchronous event occures at server**. Since we use **Apache Wicket** as framework for our front-end, there are two options for server-push pattern for us:

* Using of Wicket Native implementation of Websockets. @See [Wicket Native WebSockets](https://cwiki.apache.org/confluence/display/WICKET/Wicket+Native+WebSockets)
* Using of Wicket Atmosphere integration. 

## Wicket Native WebSockets ##

This option does not seem as a way to go for us, because it's an experimental module and most of all it doesn't work with containers like Weblogic
which we use in our company.

## Wicket Atmosphere integration ##

This certainly looks as **very good way to go**..:-), because of the following facts:

* Except of websockets it also offers options like **Http Streaming** or **Http Long Polling**.
* If you're somehow pushed to use websockets and your customer would use websockets incompatible browser, **atmosphere will do fallback for you to use Http Streaming or Http Long Polling.**
* **Most of all** it works at **WebLogic** for example, with some configuration changes that I will show later.

## Howto use Wicket Atmosphere integration ##

### Wicket Application configuration ###

In your main **Wicket application class** you typically configure** org.apache.wicket.atmosphere.EventBus**, which serves as events publisher from server to client. Let's go with example:

```
    /**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
	super.init();

	// add your configuration here
	eventBus = new EventBus(this);
        eventBus.getParameters().setTransport(AtmosphereTransport.LONG_POLLING);
        eventBus.getParameters().setLogLevel(AtmosphereLogLevel.DEBUG);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final Runnable beeper = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    eventBus.post(new Date());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };
        scheduler.scheduleWithFixedDelay(beeper, 5, 5, TimeUnit.SECONDS);
	}
```
Basicaly we're saying here that for server-push we're going to be using **LONG_POLLING** and every 5 seconds we're going to be publishing current date to all subscribers.
Now subscribers for this events can be listening in the following way, wherever in your application add following page:
```
public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	Label label;

	public HomePage(final PageParameters parameters) {
		super(parameters);
		
		label = new Label("version", new Date().toString());
		label.setOutputMarkupId(true);
		add(label);		
         }
	
	 @Subscribe
	 public void updateTime(AjaxRequestTarget target, Date event)
	 {
		label.setDefaultModelObject(event.toString());
	    target.add(label);
	 }	
}
```

# Http Streaming vs Long Polling #

Just to summarize things up, server-push ways can be divided into the following protocol categories:

* WebSockets 
* Http (Long Polling, Streaming)

## Long polling vs Streaming ##

Just to explain, when you use **Long Polling**, then client and server will establish http connection between themselfs and as soons as they will change the data the connection is closed and recreated. When using **Streaming**, client and server will establish connection at the begining and that connection is used all the time.

# Howto run Wicket Atmosphere at WebLogic12c #

Now the best thing at the end!..:-) Like I said, in our company we use **WebLogic 12c**, at least we're going to be soon, so as main Wicket technology specialist, I needed to make the Wicket Atmosphere integration work at WebLogic 12c. Well first of all, example at [Atmosphere](http://examples7x.wicket.apache.org/atmosphere) doesn't work at WebLogic12c at all. You need to make the following changes:


```
<dependency>
   <groupId>org.apache.wicket</groupId>
   <artifactId>wicket-atmosphere</artifactId>
   <version>0.18</version>
</dependency>	
		
<dependency>
  <groupId>org.atmosphere</groupId>
  <artifactId>atmosphere-weblogic</artifactId>
  <version>2.0.0.RC1</version>
</dependency>
```
You need to add own **wicket-atmosphere** module and also **atmosphere pluging for WebLogic**. From that plugin you just to need to use weblogic compatible atmosphere servlet **org.atmosphere.weblogic.AtmosphereWebLogicServlet**, your web.xml needs to be looking like this:


```
<?xml version="1.0" encoding="ISO-8859-1"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
	version="2.5">

	<display-name>myproject</display-name>

	<servlet>
		<description>Atmospherefilter</description>
		<servlet-name>Atmospherefilter</servlet-name>
		<servlet-class>org.atmosphere.weblogic.AtmosphereWebLogicServlet</servlet-class>
		<init-param>
			<param-name>applicationClassName</param-name>
			<param-value>com.mycompany.WicketApplication</param-value>
		</init-param>
		<init-param>
			<param-name>configuration</param-name>
			<param-value>development</param-value>
		</init-param>
		<init-param>
			<param-name>org.atmosphere.useWebSocket</param-name>
			<param-value>false</param-value>
		</init-param>
		<init-param>
			<param-name>org.atmosphere.useNative</param-name>
			<param-value>false</param-value>
		</init-param>
		<init-param>
			<param-name>org.atmosphere.cpr.sessionSupport</param-name>
			<param-value>true</param-value>
		</init-param>
		<init-param>
			<param-name>filterMappingUrlPattern</param-name>
			<param-value>/app/*</param-value>
		</init-param>
		<init-param>
			<param-name>org.atmosphere.websocket.WebSocketProtocol</param-name>
			<param-value>org.atmosphere.websocket.protocol.EchoProtocol</param-value>
		</init-param>
		<init-param>
			<param-name>org.atmosphere.cpr.broadcastFilterClasses</param-name>
			<param-value>org.apache.wicket.atmosphere.TrackMessageSizeFilter</param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>		
	</servlet>
	<servlet-mapping>
		<servlet-name>Atmospherefilter</servlet-name>
		<url-pattern>/app/*</url-pattern>
	</servlet-mapping>
</web-app>
```
Now if you git clone this repo, build war and deploy it to WebLogic12c, you should see the datetime refreshed every 5 seconds via pushing the time from server.
I hope you found this usefull, see you..:)

Regards

Tomas