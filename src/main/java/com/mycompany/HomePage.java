package com.mycompany;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.atmosphere.EventBus;
import org.apache.wicket.atmosphere.Subscribe;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import java.util.Date;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import org.apache.wicket.markup.html.WebPage;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	Label label;

	public HomePage(final PageParameters parameters) {
		super(parameters);
		
		label = new Label("version", new Date().toString());
		label.setOutputMarkupId(true);
		add(label);		
    }
	
	 @Subscribe
	 public void updateTime(AjaxRequestTarget target, Date event)
	 {
		 	label.setDefaultModelObject(event.toString());
	        target.add(label);
	 }	
}
